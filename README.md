# README #

**WaitNoMore** is a Queue Tracking Web-based System with Real-time SMS updates

### Background ###

In Singapore, people love to queue or line up to get the service that they need or get a spot at the hottest restaurants in town. Wouldn't it be nice if there's an app where people can subscribe via SMS or Smartphone app to get real-time updates about how long is the queue, or when their queue number gets called, or when they're about to miss their reservation? ** WaitNoMore** could do all these. Now, people won't have to line up for hours while waiting for their turn. This enables them to do what they want to do, like shopping, or having fun with friends and family, while waiting.

### Demo ###

You can find the video demo [here](https://www.youtube.com/watch?v=3hf_ELoyLOo)

### Sample SMS Message ###

Special thanks to my friend in Sydney, Australia for letting me spam her phone. This is what she got:

![received_10207356699781076.jpeg](https://bitbucket.org/repo/yz6aM7/images/2986359975-received_10207356699781076.jpeg)

### Instructions ###

To set up your local development environment, please follow the instructions below.

### System Requirements ###

1. Windows 7 or higher
2. Visual Studio 2013
3. SQL Server 2008 or higher
4. .NET 4.5

### Running the Application ###

1. Clone the source code from this repository
2. Open the **WaitNoMore** solution in Visual Studio 2013
3. Download NuGet packages by building the solution (or restore from the NuGet Package Manager)
4. Set **WaitNoMore.Web** as the StartUp project
5. Open the Package Manager Console in Visual Studio
6. Select **WaitNoMore.Data** from the **Default project** dropdown
7. On the console, type the command: *update-database*

That's it! You're good to go!

### Technologies / Libs / Frameworks ###

1. ASP.NET MVC 5
2. Bootstrap 3
3. Entity Framework 6
4. SimpleInjector
5. Telstra API Client (modified)

Initially, I considered using ASP.NET Web API 2 as an SMS Gateway callback. It's not fully implemented and it's not one of the priorities right now.

### Architecture ###

Normally, I write unit tests for projects I work on. However, for this project, I decided to leave out unit testing for now so I can submit it on time. Even though I wasn't able to write unit tests for this project, it is still absolutely unit testable. I've implemented the Repository Pattern to enable easy unit test mocking and I've used dependency injection heavily to make the project modularized. It also uses multi layered architecture.