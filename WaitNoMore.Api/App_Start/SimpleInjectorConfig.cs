﻿using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WaitNoMore.Bootstrapper;
using WaitNoMore.Domain.Models;
using WaitNoMore.Dto;

namespace WaitNoMore.Api
{
    public static class SimpleInjectorConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new Container();

            // Register other dependencies
            SimpleInjectorBootstrap.Configure(container);

            // Register controllers
            container.RegisterWebApiControllers(config);

            container.Verify();

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}