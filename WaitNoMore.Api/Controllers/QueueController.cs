﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WaitNoMore.Domain;
using WaitNoMore.Domain.Models;
using WaitNoMore.Dto;

namespace WaitNoMore.Api.Controllers
{
    [RoutePrefix("queue")]
    public class QueueController : ApiController
    {
        private readonly IQueueManager queueManager;

        public QueueController(IQueueManager queueManager)
        {
            this.queueManager = queueManager;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Enqueue(QueueRequest request)
        {
            try
            {
                // Put in mapping class
                var queuer = new Queuer
                {
                    QueueCode = request.QueueCode,
                    Requester = request.Requester,
                    Sender = request.Sender,
                    SenderType = request.SenderType,
                    Subscriptions = new List<Subscription>
                    {
                        new Subscription
                        {
                            Destination = request.Sender,
                            Type = request.SenderType
                        }
                    }
                };
                var result = this.queueManager.EnqueueInto(request.QueueCode, request.AccountCode, queuer);

                return Ok(new WaitInfoResponse
                    {
                        QueueCode = result.QueueCode,
                        QueueCount = result.QueueCount,
                        WaitInSec = result.WaitInSec
                    });
            }
            catch (ExistingQueuerException)
            {
                return Conflict();
            }
            catch (MissingQueueException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // WARNING: Delete later. Do not expose!
        [Route("Next")]
        [HttpPost]
        public IHttpActionResult CallNext(QueueRequest request)
        {
            try
            {
                var queuer = this.queueManager.CallNext(request.QueueCode, request.AccountCode);
                if (queuer == null)
                    return NotFound();

                return Ok(queuer);
            }
            catch (MissingQueuerException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
