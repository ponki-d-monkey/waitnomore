﻿using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Data;
using WaitNoMore.Domain;
using WaitNoMore.Domain.IdGenerator;
using WaitNoMore.Domain.Notification;
using WaitNoMore.Domain.Notification.Sms;
using WaitNoMore.Domain.Repositories;
using WaitNoMore.Notification.Sms;

namespace WaitNoMore.Bootstrapper
{
    public static class SimpleInjectorBootstrap
    {
        public static void Configure(Container container)
        {
            container.Register<IWaitNoMoreContext, WaitNoMoreContext>();

            container.Register<IQueueRepository, QueueRepository>();
            container.Register<IQueuerRepository, QueuerRepository>();
            container.Register<IAccountRepository, AccountRepository>();
            container.Register<IClientRepository, ClientRepository>();

            //container.Register<ISmsClient, SorryForKeepingYouAwakeMyFriendInSydneySmsClient>();
            container.Register<ISmsClient, TelstraSmsClient>();
            container.Register<INotifier, Notifier>();
            container.Register<IQNumGenerator, NaiveQNumGenerator>();
            container.Register<IQueueManager, QueueManager>();
            container.Register<IClientAdministration, ClientAdministration>();
        }
    }
}
