﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Data
{
    public sealed class AccountRepository : EfRepositoryBase, IAccountRepository
    {
        public AccountRepository(IWaitNoMoreContext context)
            : base(context)
        {

        }

        public Account FindOne(FilterContainer<Account> filters)
        {
            IQueryable<Account> accounts = Context
                .Accounts
                .Include("Queues");
            foreach (var filter in filters.Filters)
            {
                accounts = accounts.Where(filter);
            }
            return accounts.FirstOrDefault();
        }

        public IEnumerable<Account> Filter(FilterContainer<Account> filters)
        {
            IQueryable<Account> accounts = Context.Accounts;
            foreach (var filter in filters.Filters)
            {
                accounts = accounts.Where(filter);
            }
            return accounts.AsEnumerable();
        }
    }
}
