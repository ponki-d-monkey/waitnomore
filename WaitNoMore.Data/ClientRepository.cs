﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Data
{
    public sealed class ClientRepository : EfRepositoryBase, IClientRepository
    {
        public ClientRepository(IWaitNoMoreContext context)
            : base(context)
        {

        }

        public Client FindOne(FilterContainer<Client> filters)
        {
            IQueryable<Client> clients = Context.Clients;
            foreach (var filter in filters.Filters)
            {
                clients = clients.Where(filter);
            }
            return clients.FirstOrDefault();
        }

        public IEnumerable<Client> GetAll()
        {
            return Context.Clients.AsEnumerable();
        }
    }
}
