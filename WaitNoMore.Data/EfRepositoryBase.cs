﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Data
{
    public abstract class EfRepositoryBase
    {
        protected EfRepositoryBase(IWaitNoMoreContext context)
        {
            Context = context;
        }

        protected IWaitNoMoreContext Context { get; private set; }
    }
}
