namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateClients : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Clients", c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(false, 100, false, true)
                })
                .PrimaryKey(c => c.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.Clients");
        }
    }
}
