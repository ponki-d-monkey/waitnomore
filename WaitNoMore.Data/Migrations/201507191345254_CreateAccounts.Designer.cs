// <auto-generated />
namespace WaitNoMore.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CreateAccounts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateAccounts));
        
        string IMigrationMetadata.Id
        {
            get { return "201507191345254_CreateAccounts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
