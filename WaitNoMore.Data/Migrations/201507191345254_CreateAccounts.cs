namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAccounts : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Accounts", c => new
                {
                    Id = c.Int(false, true),
                    ClientId = c.Int(false),
                    Name = c.String(false, 100, false, true),
                    Code = c.String(false, Guid.Empty.ToString().Length, false, false)
                })
                .PrimaryKey(c => c.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "ClientId", "dbo.Clients");
            DropTable("dbo.Accounts");
        }
    }
}
