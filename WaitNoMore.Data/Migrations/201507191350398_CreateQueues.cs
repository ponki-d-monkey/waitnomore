namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateQueues : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Queues", c => new
                {
                    Id = c.Int(false, true),
                    AccountId = c.Int(false, false),
                    AccountCode = c.String(false, Guid.Empty.ToString().Length, false, false),
                    Name = c.String(false, 100, false, true),
                    Code = c.String(false, 100, false, false),
                    Seed = c.Int(false, false),
                    IsDefault = c.Boolean(false)
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountId, true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Queues", "AccountId", "dbo.Accounts");
            DropTable("dbo.Queues");
        }
    }
}
