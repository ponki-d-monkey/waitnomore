namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateSubscribers : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Subscribers", c => new
                {
                    Id = c.Int(false, true),
                    QueueId = c.Int(false, false),
                    Destination = c.String(false, 100, false, false),
                    Type = c.String(false, 100, false, false)
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Queues", t => t.QueueId, true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscribers", "QueueId", "dbo.Queues");
            DropTable("dbo.Subscribers");
        }
    }
}
