namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateQueuers : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Queuers", c => new
                {
                    Id = c.Int(false, true),
                    QueueId = c.Int(false, false),
                    QueueCode = c.String(false, 100, false, false),
                    Number = c.Int(false, false),
                    Requester = c.String(false, 100, false, false),
                    Status = c.String(false, 100, false, false),
                    Sender = c.String(false, 100, false, false),
                    SenderType = c.String(false, 100, false, false),
                    Created = c.DateTime(false),
                    Ended = c.DateTime(true)
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Queues", t => t.QueueId, true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Queuers", "QueueId", "dbo.Queues");
            DropTable("dbo.Queuers");
        }
    }
}
