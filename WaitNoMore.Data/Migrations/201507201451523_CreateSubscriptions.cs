namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateSubscriptions : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Subscriptions", c => new
                {
                    Id = c.Int(false, true),
                    QueuerId = c.Int(false, false),
                    Destination = c.String(false, 100, false, false),
                    Type = c.String(false, 100, false, false)
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Queuers", t => t.QueuerId, true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscriptions", "QueuerId", "dbo.Queuers");
            DropTable("dbo.Subscriptions");
        }
    }
}
