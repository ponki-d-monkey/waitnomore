namespace WaitNoMore.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WaitNoMore.Domain.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WaitNoMoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WaitNoMore.Data.WaitNoMoreContext context)
        {
#if DEBUG
            var utcNow = DateTime.UtcNow;
            var queuer1 = new Queuer
            {
                QueueCode = "N",
                Number = 1000,
                Requester = "James Buchanan",
                Sender = "",
                SenderType = "manual",
                Status = "done",
                Created = utcNow.AddMinutes(-10D),
                Ended = utcNow.AddMinutes(-5D)
            };
            var queuer2 = new Queuer
            {
                QueueCode = "N",
                Number = 1001,
                Requester = "John Doe",
                Sender = "",
                SenderType = "manual",
                Status = "done",
                Created = utcNow.AddMinutes(-7D),
                Ended = utcNow
            };
            var queuer3 = new Queuer
            {
                QueueCode = "N",
                Number = 1002,
                Requester = "Mary Jane",
                Sender = "",
                SenderType = "manual",
                Status = "current",
                Created = utcNow.AddMinutes(-5D)
            };
            var queuer4 = new Queuer
            {
                QueueCode = "N",
                Number = 1003,
                Requester = "Juan Dela Cruz",
                Sender = "",
                SenderType = "manual",
                Status = "pending",
                Created = utcNow.AddMinutes(-2D)
            };
            var queuer5 = new Queuer
            {
                QueueCode = "N",
                Number = 1004,
                Requester = "Banh Mi Nguyen",
                Sender = "",
                SenderType = "manual",
                Status = "pending",
                Created = utcNow
            };

            var q1 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "RMBN",
                Queuers = new List<Queuer>
                {
                    queuer1,
                    queuer2,
                    queuer3,
                    queuer4,
                    queuer5
                }
            };
            var q2 = new Queue
            {
                Name = "Priority",
                Code = "P",
                Seed = 9000,
                IsDefault = false,
                AccountCode = "RMBN"
            };
            var q3 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "RMT"
            };
            var q4 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "RMC"
            };
            var account1 = new Account
            {
                Name = "Raffles Medical - Bedok North",
                Code = "RMBN",
                Queues = new List<Queue>
                {
                    q1,
                    q2
                }
            };
            var account2 = new Account
            {
                Name = "Raffles Medical - Tampines",
                Code = "RMT",
                Queues = new List<Queue>
                {
                    q3
                }
            };
            var account3 = new Account
            {
                Name = "Raffles Medical - Central (City Hall)",
                Code = "RMC",
                Queues = new List<Queue>
                {
                    q4
                }
            };
            var client1 = new Client
            {
                Name = "Raffles Medical Pte Ltd",
                Accounts = new List<Account>
                {
                    account1,
                    account2,
                    account3
                }
            };

            var q5 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "THWB"
            };
            var q6 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "THWO"
            };
            var q7 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "THWJ"
            };
            var q8 = new Queue
            {
                Name = "Normal",
                Code = "N",
                Seed = 1000,
                IsDefault = true,
                AccountCode = "THWW"
            };

            var account4 = new Account
            {
                Name = "Tim Ho Wan - Bedok",
                Code = "THWB",
                Queues = new List<Queue>
                {
                    q5
                }
            };
            var account5 = new Account
            {
                Name = "Tim Ho Wan - Orchard",
                Code = "THWO",
                Queues = new List<Queue>
                {
                    q6
                }
            };
            var account6 = new Account
            {
                Name = "Tim Ho Wan - Jurong",
                Code = "THWO",
                Queues = new List<Queue>
                {
                    q7
                }
            };
            var account7 = new Account
            {
                Name = "Tim Ho Wan - Woodlands",
                Code = "THWJ",
                Queues = new List<Queue>
                {
                    q8
                }
            };
            var client2 = new Client
            {
                Name = "Tim Ho Wan Pte Ltd",
                Accounts = new List<Account>
                {
                    account4,
                    account5,
                    account6,
                    account7
                }
            };
            

            context.Clients.AddOrUpdate(entity => entity.Name, client1, client2);
            context.SaveChanges();
#endif
        }
    }
}
