﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Data
{
    public sealed class MissingQueuerException : ApplicationException
    {
        public MissingQueuerException(int qNum, string message)
            : base(message)
        {
            QNum = qNum;
        }

        public int QNum { get; private set; }
    }
}
