﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Data
{
    public class QueueRepository : EfRepositoryBase, IQueueRepository
    {
        public QueueRepository(IWaitNoMoreContext context)
            : base(context)
        {

        }

        public Queue FindOne(FilterContainer<Queue> filters, params string[] includes)
        {
            IQueryable<Queue> queues = Context.Queues;
            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    queues = queues.Include(include);
                }
            }
            foreach (var filter in filters.Filters)
            {
                queues = queues.Where(filter);
            }
            return queues.FirstOrDefault();
        }
    }
}
