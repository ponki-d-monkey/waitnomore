﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Data
{
    public sealed class QueuerRepository : EfRepositoryBase, IQueuerRepository
    {
        public QueuerRepository(IWaitNoMoreContext context)
            : base(context)
        {

        }

        public Queuer Add(Queuer queuer)
        {
            Context.Queuers.Add(queuer);
            Context.SaveChanges();
            return queuer;
        }

        public Queuer Update(Queuer queuer)
        {
            var existing = Context.Queuers.FirstOrDefault(q => q.Id == queuer.Id);
            if (existing == null)
                return null;

            // TODO: Set properties 1 by 1.

            Context.SaveChanges();
            return existing;
        }

        public Queuer FindOne(FilterContainer<Queuer> filters, params string[] includes)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    queuers = queuers.Include(include);
                }
            }
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return queuers.FirstOrDefault();
        }

        public async Task<Queuer> FindOneAsync(FilterContainer<Queuer> filters, params string[] includes)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    queuers = queuers.Include(include);
                }
            }
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return await queuers.FirstOrDefaultAsync();
        }

        public Queuer FindOne<TKey>(FilterContainer<Queuer> filters, Expression<Func<Queuer, TKey>> orderBy)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            queuers = queuers.OrderBy(orderBy);
            return queuers.FirstOrDefault();
        }

        public IEnumerable<Queuer> Filter(FilterContainer<Queuer> filters, params string[] includes)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    queuers = queuers.Include(include);
                }
            }
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return queuers;
        }

        public async Task<IEnumerable<Queuer>> FilterAsync(FilterContainer<Queuer> filters, params string[] includes)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    queuers = queuers.Include(include);
                }
            }
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return await queuers.ToListAsync();
        }

        public IEnumerable<Queuer> Filter<TKey>(FilterContainer<Queuer> filters, Expression<Func<Queuer, TKey>> orderBy)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            queuers = queuers.OrderBy(orderBy);
            return queuers;
        }

        public int Count(FilterContainer<Queuer> filters)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return queuers.Count();
        }

        public double? Average(FilterContainer<Queuer> filters)
        {
            IQueryable<Queuer> queuers = Context.Queuers;
            foreach (var filter in filters.Filters)
            {
                queuers = queuers.Where(filter);
            }
            return queuers.Average(q => DbFunctions.DiffSeconds(q.Created, q.Ended));
        }
    }
}
