﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Data
{
    public sealed class SubscriptionRepository : EfRepositoryBase, ISubscriptionRepository
    {
        public SubscriptionRepository(IWaitNoMoreContext context)
            : base(context)
        {

        }

        public IEnumerable<Subscription> Filter(FilterContainer<Subscription> filters)
        {
            IQueryable<Subscription> subscriptions = Context.Subscriptions;
            foreach (var filter in filters.Filters)
            {
                subscriptions = subscriptions.Where(filter);
            }
            return subscriptions;
        }
    }
}
