﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Data
{
    public interface IWaitNoMoreContext : IDisposable
    {
        DbSet<Client> Clients { get; set; }
        DbSet<Account> Accounts { get; set; }
        DbSet<Queue> Queues { get; set; }
        DbSet<Queuer> Queuers { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }

        int SaveChanges();
    }

    [DbConfigurationType(typeof(DbContextConfiguration))]
    public partial class WaitNoMoreContext : DbContext, IWaitNoMoreContext
    {
        public WaitNoMoreContext()
            : base("WaitNoMore")
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Queue> Queues { get; set; }
        public DbSet<Queuer> Queuers { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
    }
}
