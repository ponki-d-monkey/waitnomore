﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Data
{
    public partial class WaitNoMoreContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Clients
            modelBuilder.Entity<Client>()
                .HasKey(entity => entity.Id)
                .HasMany(entity => entity.Accounts)
                .WithRequired(entity => entity.Client)
                .HasForeignKey(entity => entity.ClientId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Client>()
                .Property(entity => entity.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            modelBuilder.Entity<Client>()
                .Property(entity => entity.Name)
                .IsRequired()
                .IsUnicode()
                .IsVariableLength()
                .HasMaxLength(100);
            #endregion

            #region Accounts
            modelBuilder.Entity<Account>()
                .HasKey(entity => entity.Id)
                .HasMany(entity => entity.Queues)
                .WithRequired(entity => entity.Account)
                .HasForeignKey(entity => entity.AccountId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Account>()
                .Property(entity => entity.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(entity => entity.Name)
                .IsRequired()
                .IsUnicode()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Account>()
                .Property(entity => entity.Code)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(Guid.Empty.ToString().Length);

            modelBuilder.Entity<Account>()
                .Property(entity => entity.ClientId)
                .IsRequired();
            #endregion

            #region Queues
            modelBuilder.Entity<Queue>()
                .HasKey(entity => entity.Id)
                .HasMany(entity => entity.Queuers)
                .WithRequired(entity => entity.Queue)
                .HasForeignKey(entity => entity.QueueId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.Name)
                .IsRequired()
                .IsUnicode()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.Code)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.Seed)
                .IsRequired();

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.AccountId)
                .IsRequired();

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.AccountCode)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queue>()
                .Property(entity => entity.IsDefault)
                .IsRequired();
            #endregion

            #region Queuers
            modelBuilder.Entity<Queuer>()
                .HasKey(entity => entity.Id)
                .HasMany(entity => entity.Subscriptions)
                .WithRequired(entity => entity.Queuer)
                .HasForeignKey(entity => entity.QueuerId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Number)
                .IsRequired();

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Requester)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Sender)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.SenderType)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Status)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.QueueId)
                .IsRequired();

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.QueueCode)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Created)
                .IsRequired();

            modelBuilder.Entity<Queuer>()
                .Property(entity => entity.Ended)
                .IsOptional();
            #endregion

            #region Subscribers
            modelBuilder.Entity<Subscription>()
                .HasKey(entity => entity.Id)
                .HasRequired(entity => entity.Queuer);

            modelBuilder.Entity<Subscription>()
                .Property(entity => entity.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            modelBuilder.Entity<Subscription>()
                .Property(entity => entity.QueuerId)
                .IsRequired();

            modelBuilder.Entity<Subscription>()
                .Property(entity => entity.Type)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);

            modelBuilder.Entity<Subscription>()
                .Property(entity => entity.Destination)
                .IsRequired()
                .IsVariableLength()
                .HasMaxLength(100);
            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
