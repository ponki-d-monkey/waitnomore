﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Domain
{
    public interface IClientAdministration
    {
        Client GetClientBy(int id);
        IEnumerable<Client> GetClients();
        Account GetAccountBy(int id);
        IEnumerable<Account> GetAccountsBy(int clientId);
    }

    public sealed class ClientAdministration : IClientAdministration
    {
        private readonly IClientRepository clientRepository;
        private readonly IAccountRepository accountRepository;

        public ClientAdministration(
            IClientRepository clientRepository,
            IAccountRepository accountRepository)
        {
            this.clientRepository = clientRepository;
            this.accountRepository = accountRepository;
        }

        public Client GetClientBy(int id)
        {
            var container = FilterContainer<Client>.Create();
            container.Filters.Add(c => c.Id == id);
            return this.clientRepository.FindOne(container);
        }

        public IEnumerable<Client> GetClients()
        {
            return this.clientRepository.GetAll();
        }

        public Account GetAccountBy(int id)
        {
            var container = FilterContainer<Account>.Create();
            container.Filters.Add(a => a.Id == id);
            return this.accountRepository.FindOne(container);
        }

        public IEnumerable<Account> GetAccountsBy(int clientId)
        {
            var container = FilterContainer<Account>.Create();
            container.Filters.Add(a => a.ClientId == clientId);
            return this.accountRepository.Filter(container);
        }
    }
}
