﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Domain
{
    public sealed class ExistingQueuerException : ApplicationException
    {
        public ExistingQueuerException(Queuer existing, string message)
            : base(message)
        {
            Queuer = existing;
        }

        public Queuer Queuer { get; set; }
    }
}
