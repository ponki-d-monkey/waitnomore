﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.IdGenerator
{
    public interface IQNumGenerator
    {
        int Generate(int seed, Hashtable options);
    }
}
