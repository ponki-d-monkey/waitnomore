﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Repositories;

using Queue = WaitNoMore.Domain.Models.Queue;

namespace WaitNoMore.Domain.IdGenerator
{
    public sealed class NaiveQNumGenerator : IQNumGenerator
    {
        private readonly IQueuerRepository queuerRepository;

        public NaiveQNumGenerator(IQueuerRepository queueRepository)
        {
            this.queuerRepository = queueRepository;
        }

        public int Generate(int seed, Hashtable options)
        {
            var filters = CreateFilters(options);
            var queuers = this.queuerRepository.Filter(filters);
            return queuers.Count() == 0 ? seed : queuers.Max(q => q.Number) + 1;
        }

        private FilterContainer<Queuer> CreateFilters(Hashtable options)
        {
            var utcToday = DateTime.UtcNow.Date;
            var container = FilterContainer<Queuer>.Create();
            if (options != null)
            {
                if (options.ContainsKey("AccountCode"))
                {
                    var accountCode = options["AccountCode"].ToString();
                    container.Filters.Add(q => q.Queue.Account.Code == accountCode);
                }
                if (options.ContainsKey("QueueCode"))
                {
                    var qCode = options["QueueCode"].ToString();
                    container.Filters.Add(q => q.QueueCode == qCode);
                }
            }
            container.Filters.Add(q => q.Created >= utcToday);
            return container;
        }
    }
}
