﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain
{
    public sealed class MissingQueueException : ApplicationException
    {
        public MissingQueueException(string qCode, string message)
            : base(message)
        {
            QCode = qCode;
        }

        public string QCode { get; private set; }
    }
}
