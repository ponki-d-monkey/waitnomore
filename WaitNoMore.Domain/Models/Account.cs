﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Models
{
    public sealed class Account
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public Client Client { get; set; }
        public ICollection<Queue> Queues { get; set; }
    }
}
