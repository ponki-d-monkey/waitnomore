﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Models
{
    public sealed class Queue
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string AccountCode { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Seed { get; set; }
        public bool IsDefault { get; set; }

        public Account Account { get; set; }
        public ICollection<Queuer> Queuers { get; set; }
    }
}
