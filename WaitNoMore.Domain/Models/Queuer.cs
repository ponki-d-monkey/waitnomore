﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Models
{
    public sealed class Queuer
    {
        public int Id { get; set; }
        public int QueueId { get; set; }
        public string QueueCode { get; set; }
        public int Number { get; set; }
        public string Requester { get; set; }
        // Pending, Current, Done, Expired
        public string Status { get; set; }
        public string Sender { get; set; }
        // SMS, Email, App, Manual
        public string SenderType { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Ended { get; set; }

        public Queue Queue { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
    }
}
