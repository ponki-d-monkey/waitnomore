﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Models
{
    public sealed class Subscription
    {
        public int Id { get; set; }
        public int QueuerId { get; set; }
        public string Destination { get; set; }
        // SMS, Email, App, Manual
        public string Type { get; set; }

        public Queuer Queuer { get; set; }
    }
}
