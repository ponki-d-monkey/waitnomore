﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Models
{
    public sealed class WaitInfo
    {
        public string QueueCode { get; set; }
        public int QueueCount { get; set; }
        public double? WaitInSec { get; set; }
    }
}
