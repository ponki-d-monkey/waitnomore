﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Notification.Sms;
using WaitNoMore.Domain.Repositories;

namespace WaitNoMore.Domain.Notification
{
    public interface INotifier
    {
        Task SmsQueuerByAsync(int id, string message);
        Task SmsPendingQueuersAsync(string qCode, string accountCode, string message);
    }

    public sealed class Notifier : INotifier
    {
        private readonly ISmsClient smsClient;
        private readonly IQueuerRepository queuerRepository;
        private readonly IQueueManager queueManager;

        public Notifier(
            ISmsClient smsClient,
            IQueuerRepository queuerRepository,
            IQueueManager queueManager)
        {
            this.smsClient = smsClient;
            this.queuerRepository = queuerRepository;
            this.queueManager = queueManager;
        }

        public async Task SmsQueuerByAsync(int id, string message)
        {
            var container = FilterContainer<Queuer>.Create();
            container.Filters.Add(q => q.Id == id);

            var queuer = await this.queuerRepository.FindOneAsync(container, "Subscriptions");
            if (queuer != null && queuer.Subscriptions != null && queuer.Subscriptions.Any())
            {
                await NotifyQueuerAsync(message, queuer);
            }
        }

        public async Task SmsPendingQueuersAsync(string qCode, string accountCode, string message)
        {
            var queuers = await GetSubscribersAsync(qCode, accountCode);
            if (queuers != null && queuers.Any())
            {
                var waitInfo = this.queueManager.GetWaitInfo(qCode, accountCode);
                foreach (var queuer in queuers)
                {
                    await NotifyQueuerAsync(message, queuer, waitInfo);
                }
            }
        }

        private async Task<IEnumerable<Queuer>> GetSubscribersAsync(string qCode, string accountCode)
        {
            var utcToday = DateTime.UtcNow.Date;
            var container = FilterContainer<Queuer>.Create();
            container.Filters.Add(q => q.Queue.Code == qCode);
            container.Filters.Add(q => q.Queue.Account.Code == accountCode);
            container.Filters.Add(q => q.Subscriptions.Any());
            container.Filters.Add(q => q.Status == "pending");
            container.Filters.Add(q => q.Created >= utcToday);
            var queuers = await this.queuerRepository.FilterAsync(container, "Subscriptions");
            return queuers;
        }

        private async Task NotifyQueuerAsync(string message, Queuer queuer, WaitInfo waitInfo = null)
        {
            if (queuer.Subscriptions == null || !queuer.Subscriptions.Any())
                return;

            var subscription = queuer.Subscriptions.FirstOrDefault(s => s.Type == "sms");
            if (subscription == null)
                return;

            if (string.IsNullOrWhiteSpace(subscription.Destination))
                return;

            var smsMessage = BuildMessage(message, waitInfo);
            await this.smsClient.SendAsync(subscription.Destination, smsMessage);
        }

        private string BuildMessage(string message, WaitInfo waitInfo)
        {
            var messageBuilder = new StringBuilder(message);
            if (waitInfo == null)
                return messageBuilder.ToString();

            messageBuilder.Append(" / ");
            messageBuilder.AppendFormat("Queue: {0} / ", waitInfo.QueueCode);
            messageBuilder.AppendFormat("Pending: {0} / ", waitInfo.QueueCount);
            messageBuilder.AppendFormat("Average Wait: {0} / ", waitInfo.WaitInSec.HasValue ? waitInfo.WaitInSec.Value.ToString() : "N/A");
            return messageBuilder.ToString();
        }
    }
}
