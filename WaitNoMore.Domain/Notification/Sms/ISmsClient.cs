﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Notification.Sms
{
    public interface ISmsClient
    {
        bool Send(string recipientNumber, string message);

        Task<bool> SendAsync(string recipientNumber, string message);
    }
}
