﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Notification.Sms
{
    public abstract class SmsClientBase : ISmsClient
    {
        public bool Send(string recipientNumber, string message)
        {
            if (string.IsNullOrWhiteSpace(recipientNumber))
                throw new ArgumentException("'Recipient Number' must not be empty.", "recipientNumber");
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentException("'Message' must not be empty.", "message");

            if (message.Length > 160)
                throw new ArgumentOutOfRangeException("message", "'Message' must not exceed 160 characters.");

            return DoSend(recipientNumber, message);
        }

        public Task<bool> SendAsync(string recipientNumber, string message)
        {
            return Task.Run<bool>(() =>
            {
                return Send(recipientNumber, message);
            });
        }

        protected abstract bool DoSend(string recipientNumber, string message);
    }
}
