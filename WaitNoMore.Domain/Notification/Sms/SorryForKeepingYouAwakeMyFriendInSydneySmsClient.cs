﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Notification.Sms
{
    public sealed class SorryForKeepingYouAwakeMyFriendInSydneySmsClient : SmsClientBase
    {
        protected override bool DoSend(string recipientNumber, string message)
        {
            Debug.WriteLine("Recipient Number: {0}", recipientNumber);
            Debug.WriteLine("Message: {0}", message);
            return true;
        }
    }
}
