﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.IdGenerator;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Notification;
using WaitNoMore.Domain.Repositories;
using Queue = WaitNoMore.Domain.Models.Queue;

namespace WaitNoMore.Domain
{
    public interface IQueueManager
    {
        Queue GetQueueBy(int id);
        WaitInfo EnqueueInto(string qCode, string accountCode, Queuer queuer);
        WaitInfo GetWaitInfo(string qCode, string accountCode);
        Queuer CallNext(string qCode, string accountCode);
        bool Skip(int queuerId);
        bool Finish(int queuerId);
    }

    public sealed class QueueManager : IQueueManager
    {
        #region Fields
        private readonly IQNumGenerator qNumGenerator;
        private readonly IQueueRepository qRepository;
        private readonly IQueuerRepository queuerRepository;
        #endregion

        #region Ctors
        public QueueManager(
            IQNumGenerator queueGenerator,
            IQueueRepository qRepository,
            IQueuerRepository queuerRepository)
        {
            this.qNumGenerator = queueGenerator;
            this.qRepository = qRepository;
            this.queuerRepository = queuerRepository;
        }
        #endregion

        #region Operations
        public Queue GetQueueBy(int id)
        {
            var container = FilterContainer<Queue>.Create();
            container.Filters.Add(q => q.Id == id);
            return this.qRepository.FindOne(container, "Queuers");
        }

        public WaitInfo EnqueueInto(string qCode, string accountCode, Queuer queuer)
        {
            try
            {
                var qFilterContainer = CreateQueueFiltersForEnqueueInto(qCode, accountCode);
                var q = this.qRepository.FindOne(qFilterContainer);
                if (q == null)
                {
                    throw new MissingQueueException(qCode, "Missing Queue");
                }

                //var queuerFilterContainer = CreateQueuerFiltersForEnqueueInto(accountCode, queuer);
                //var existingQueuer = this.queuerRepository.FindOne(queuerFilterContainer);
                //if (existingQueuer != null)
                //{
                //    throw new ExistingQueuerException(queuer, "Queuer already exists");
                //}

                queuer.QueueId = q.Id;
                queuer.Status = "pending";
                queuer.Created = DateTime.UtcNow;
                queuer.Number = this.qNumGenerator.Generate(q.Seed, new Hashtable
                { 
                    { "QueueCode" , qCode },
                    { "AccountCode", accountCode } 
                });
                if (string.IsNullOrWhiteSpace(queuer.Sender))
                {
                    queuer.Sender = string.Empty;
                }

                this.queuerRepository.Add(queuer);

                return GetWaitInfo(queuer.QueueCode, accountCode);
            }
            catch (MissingQueueException)
            {
                throw;
            }
            catch (ExistingQueuerException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DomainException("An error occurred while adding a new queuer.", ex);
            }
        }

        public WaitInfo GetWaitInfo(string qCode, string accountCode)
        {
            try
            {
                var queueCountFilterContainer = CreateQueuerCountFiltersForGetWaitInfo(qCode, accountCode);
                var count = this.queuerRepository.Count(queueCountFilterContainer);

                var averageFilterContainer = CreateQueuerAverageFiltersForGetWaitInfo(qCode, accountCode);
                var waitInSec = this.queuerRepository.Average(averageFilterContainer);

                return new WaitInfo
                {
                    QueueCode = qCode,
                    QueueCount = count,
                    WaitInSec = waitInSec
                };
            }
            catch (Exception ex)
            {
                throw new DomainException("An error occurred while getting status.", ex);
            }
        }

        public Queuer CallNext(string qCode, string accountCode)
        {
            try
            {
                var queuerFilter = CreateQueuerFiltersForCallNext(qCode, accountCode);
                var nextQueuer = this.queuerRepository.FindOne(queuerFilter, q => q.Number);
                if (nextQueuer != null)
                {
                    nextQueuer.Status = "current";
                    var existing = this.queuerRepository.Update(nextQueuer);
                    if (existing == null)
                    {
                        throw new MissingQueuerException(nextQueuer.Number, "Missing Q number");
                    }
                }

                return nextQueuer;
            }
            catch (MissingQueuerException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DomainException("An error occurred while calling the next queuer.", ex);
            }
        }

        public bool Skip(int queuerId)
        {
            try
            {
                var container = FilterContainer<Queuer>.Create();
                container.Filters.Add(q => q.Id == queuerId);
                var queuer = this.queuerRepository.FindOne(container);
                if (queuer != null)
                {
                    queuer.Status = "expired";
                    queuer.Ended = DateTime.UtcNow;
                    this.queuerRepository.Update(queuer);
                }
                return queuer != null;
            }
            catch (Exception ex)
            {
                throw new DomainException("An error occurred while skipping the given queuer.", ex);
            }
        }

        public bool Finish(int queuerId)
        {
            try
            {
                var container = FilterContainer<Queuer>.Create();
                container.Filters.Add(q => q.Id == queuerId);
                var queuer = this.queuerRepository.FindOne(container);
                if (queuer != null)
                {
                    queuer.Status = "done";
                    queuer.Ended = DateTime.UtcNow;
                    this.queuerRepository.Update(queuer);
                }
                return queuer != null;
            }
            catch (Exception ex)
            {
                throw new DomainException("An error occurred while completing a queuer's session.", ex);
            }
        }
        #endregion

        #region Private
        private FilterContainer<Queue> CreateQueueFiltersForEnqueueInto(string qCode, string accountCode)
        {
            var container = FilterContainer<Queue>.Create();
            container.Filters.Add(q => q.AccountCode == accountCode);
            if (string.IsNullOrWhiteSpace(qCode))
            {
                container.Filters.Add(q => q.IsDefault);
            }
            else
            {
                container.Filters.Add(q => q.Code == qCode);
            }
            return container;
        }

        //private FilterContainer<Queuer> CreateQueuerFiltersForEnqueueInto(string accountCode, Queuer queuer)
        //{
        //    var utcToday = DateTime.UtcNow.Date;
        //    var container = FilterContainer<Queuer>.Create();
        //    container.Filters.Add(q => q.Queue.Account.Code == accountCode);
        //    container.Filters.Add(q => q.Queue.Code == queuer.QueueCode);
        //    if (!string.IsNullOrWhiteSpace(queuer.Sender))
        //        container.Filters.Add(q => q.Sender == queuer.Sender);
        //    container.Filters.Add(q => q.Status == "pending" || q.Status == "current");
        //    container.Filters.Add(q => q.Created >= utcToday);
        //    container.Filters.Add(q => q.Ended == null);
        //    return container;
        //}

        private FilterContainer<Queuer> CreateQueuerCountFiltersForGetWaitInfo(string qCode, string accountCode)
        {
            var utcToday = DateTime.UtcNow.Date;
            var container = FilterContainer<Queuer>.Create();
            container.Filters.Add(q => q.Queue.Account.Code == accountCode);
            container.Filters.Add(q => q.QueueCode == qCode);
            container.Filters.Add(q => q.Status == "pending");
            container.Filters.Add(q => q.Created >= utcToday);
            container.Filters.Add(q => q.Ended == null);
            return container;
        }

        private FilterContainer<Queuer> CreateQueuerAverageFiltersForGetWaitInfo(string qCode, string accountCode)
        {
            var utcToday = DateTime.UtcNow.Date;
            var container = FilterContainer<Queuer>.Create();
            container.Filters.Add(q => q.Queue.Account.Code == accountCode);
            container.Filters.Add(q => q.QueueCode == qCode);
            container.Filters.Add(q => q.Status == "done");
            container.Filters.Add(q => q.Ended != null);
            return container;
        }

        private FilterContainer<Queuer> CreateQueuerFiltersForCallNext(string qCode, string accountCode)
        {
            var utcToday = DateTime.UtcNow.Date;
            var container = FilterContainer<Queuer>.Create();
            container.Filters.Add(q => q.Queue.Account.Code == accountCode);
            container.Filters.Add(q => q.QueueCode == qCode);
            container.Filters.Add(q => q.Status == "pending");
            container.Filters.Add(q => q.Created >= utcToday);
            container.Filters.Add(q => q.Ended == null);
            return container;
        }
        #endregion
    }
}
