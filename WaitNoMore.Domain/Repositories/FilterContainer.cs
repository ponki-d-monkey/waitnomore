﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Domain.Repositories
{
    public class FilterContainer<T>
    {
        private readonly List<Expression<Func<T, bool>>> filters;

        protected FilterContainer()
        {
            filters = new List<Expression<Func<T, bool>>>();
        }

        public List<Expression<Func<T, bool>>> Filters
        {
            get
            {
                return this.filters;
            }
        }

        public static FilterContainer<T> Create()
        {
            return new FilterContainer<T>();
        }
    }
}
