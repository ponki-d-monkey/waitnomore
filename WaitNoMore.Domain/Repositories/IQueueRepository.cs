﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Domain.Repositories
{
    public interface IQueueRepository
    {
        Queue FindOne(FilterContainer<Queue> filters, params string[] includes);
    }
}
