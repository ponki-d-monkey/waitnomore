﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Domain.Repositories
{
    public interface IQueuerRepository
    {
        Queuer Add(Queuer queuer);
        Queuer Update(Queuer queuer);
        Queuer FindOne(FilterContainer<Queuer> filters, params string[] includes);
        Task<Queuer> FindOneAsync(FilterContainer<Queuer> filters, params string[] includes);
        Queuer FindOne<TKey>(FilterContainer<Queuer> filters, Expression<Func<Queuer, TKey>> orderBy);
        IEnumerable<Queuer> Filter(FilterContainer<Queuer> filters, params string[] includes);
        Task<IEnumerable<Queuer>> FilterAsync(FilterContainer<Queuer> filters, params string[] includes);
        IEnumerable<Queuer> Filter<TKey>(FilterContainer<Queuer> filters, Expression<Func<Queuer, TKey>> orderBy);
        int Count(FilterContainer<Queuer> filters);
        double? Average(FilterContainer<Queuer> filters);
    }
}
