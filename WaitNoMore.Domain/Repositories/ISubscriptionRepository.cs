﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaitNoMore.Domain.Models;

namespace WaitNoMore.Domain.Repositories
{
    public interface ISubscriptionRepository
    {
        IEnumerable<Subscription> Filter(FilterContainer<Subscription> filters);
    }
}
