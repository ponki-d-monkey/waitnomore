﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Dto
{
    public sealed class QueueRequest
    {
        // Use codes for SMS and/or Email subscribers
        public string AccountCode { get; set; }
        public string QueueCode { get; set; }
        public string Requester { get; set; }
        public string Sender { get; set; }
        public string SenderType { get; set; }
        public List<Subscriber> Subscribers { get; set; }
    }
}
