﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Dto
{
    public sealed class QueuerResponse
    {
        public string QueueCode { get; set; }
        public int Number { get; set; }
        public string Requester { get; set; }
        // Pending, Current, Done, Expired
        public string Status { get; set; }
        public string Sender { get; set; }
        // SMS, Email, App, Manual
        public string SenderType { get; set; }
    }
}
