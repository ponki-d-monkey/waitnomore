﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaitNoMore.Dto
{
    public sealed class Subscriber
    {
        public string Destination { get; set; }
        public string Type { get; set; }
    }
}
