﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelstraSMSAUS.PCL.Controllers;
using TelstraSMSAUS.PCL.Models;
using WaitNoMore.Domain.Notification.Sms;
using Config = System.Configuration.ConfigurationManager;

namespace WaitNoMore.Notification.Sms
{
    public sealed class TelstraSmsClient : SmsClientBase
    {
        private static string consumerKey = Config.AppSettings.AllKeys.Contains("ConsumerKey") ?
            Config.AppSettings["ConsumerKey"] : "wRACl4KPoTGZp45i4im5bQGo7JpuODUK";

        private static string consumerSecret = Config.AppSettings.AllKeys.Contains("ConsumerSecret") ?
            Config.AppSettings["ConsumerSecret"] : "mIFjlqcqYImwy8pR";

        protected override bool DoSend(string recipientNumber, string message)
        {
            return CallTelstraApi(recipientNumber, message).Result;
        }

        private async Task<bool> CallTelstraApi(string recipientNumber, string message)
        {
            var client = TelstraClient.Instance;
            var authResponse = await client.GetAuthenticationAsync(consumerKey, consumerSecret);

            var sanitized = SanitizeAustraliaNumber(recipientNumber);
            var request = new SendMessageRequest
            {
                To = recipientNumber,
                Body = message
            };
            var smsResponse = await client.CreateSMSMessageAsync(authResponse.AccessToken, request);
            var statusResponse = await client.GetMessageStatusAsync(authResponse.AccessToken, smsResponse.MessageId);
            return statusResponse.Status == ResponseStatusEnum.SENT;
        }

        private string SanitizeAustraliaNumber(string recipientNumber)
        {
            if (recipientNumber.StartsWith("04"))
                return recipientNumber;
            if (recipientNumber.StartsWith("4"))
                return recipientNumber.Insert(0, "0");
            return recipientNumber.Insert(0, "04");
        }
    }
}
