﻿using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaitNoMore.Bootstrapper;

namespace WaitNoMore.Web
{
    public static class SimpleInjectorConfig
    {
        public static void Config()
        {
            var container = new Container();
            SimpleInjectorBootstrap.Configure(container);
            container.RegisterMvcControllers();
            container.RegisterMvcIntegratedFilterProvider();
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}