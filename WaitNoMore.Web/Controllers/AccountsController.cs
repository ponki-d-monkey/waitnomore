﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaitNoMore.Domain;
using WaitNoMore.Web.Models;

namespace WaitNoMore.Web.Controllers
{
    public class AccountsController : Controller
    {
        private readonly IClientAdministration clientAdministration;

        public AccountsController(IClientAdministration clientAdministration)
        {
            this.clientAdministration = clientAdministration;
        }

        // GET: Accounts
        public ActionResult Index(int id)
        {
            var vm = new AccountViewModel();
            var account = this.clientAdministration.GetAccountBy(id);
            if (account != null)
            {
                vm.Id = account.Id;
                vm.Name = account.Name;
                vm.Code = account.Code;
                vm.Queues = account.Queues == null ? Enumerable.Empty<QueueViewModel>() :
                    account.Queues.Select(q => new QueueViewModel
                    {
                        Id = q.Id,
                        Name = q.Name,
                        Code = q.Code
                    });
            }
            return View(vm);
        }
    }
}