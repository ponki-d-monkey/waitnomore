﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaitNoMore.Domain;
using WaitNoMore.Domain.Models;
using WaitNoMore.Web.Models;

namespace WaitNoMore.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClientAdministration clientAdministration;

        public HomeController(IClientAdministration clientAdministration)
        {
            this.clientAdministration = clientAdministration;
        }

        public ActionResult Index(int? selectedClient)
        {
            var clients = this.clientAdministration.GetClients();
            var vm = new ClientSelectionViewModel();
            if (clients != null && clients.Any())
            {
                vm.Clients = new SelectList(clients, "Id", "Name");
                if (selectedClient.HasValue)
                {
                    var accounts = this.clientAdministration.GetAccountsBy(selectedClient.GetValueOrDefault());
                    if (accounts != null && accounts.Any())
                    {
                        vm.Accounts = new SelectList(accounts, "Id", "Name");
                    }
                }
                else
                {
                    vm.Accounts = new SelectList(Enumerable.Empty<Account>(), "Id", "Name");
                }
                return View(vm);
            }
            return View(vm);
        }

        public ActionResult Account(int selectedAccount)
        {
            return RedirectToAction("Index", "Accounts", new { id = selectedAccount });
        }
    }
}