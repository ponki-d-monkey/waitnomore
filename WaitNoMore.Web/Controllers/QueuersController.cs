﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaitNoMore.Domain;
using WaitNoMore.Domain.Models;
using WaitNoMore.Web.Models;

namespace WaitNoMore.Web.Controllers
{
    public class QueuersController : Controller
    {
        private readonly IQueueManager queueManager;
        private readonly IClientAdministration clientAdministration;

        public QueuersController(
            IQueueManager queueManager,
            IClientAdministration clientAdministration)
        {
            this.queueManager = queueManager;
            this.clientAdministration = clientAdministration;
        }

        public ActionResult New(int qId)
        {
            var vm = new QueuerViewModel();
            var queue = this.queueManager.GetQueueBy(qId);
            if (queue != null)
            {
                vm.Queue = new QueueViewModel
                {
                    Id = queue.Id,
                    Name = queue.Name,
                    Code = queue.Code,
                    Queuers = queue.Queuers.Select(q => new QueuerViewModel
                    {
                        Id = q.Id,
                        Number = q.Number,
                        Requester = q.Requester,
                        Status = q.Status,
                        Sender = q.Sender,
                        SenderType = q.SenderType
                    })
                };

                var account = this.clientAdministration.GetAccountBy(queue.AccountId);
                if (account != null)
                {
                    vm.Queue.Account = new AccountViewModel
                    {
                        Id = account.Id,
                        Name = account.Name,
                        Code = account.Code,
                        Queues = account.Queues == null ? Enumerable.Empty<QueueViewModel>() :
                            account.Queues.Select(q => new QueueViewModel
                            {
                                Id = q.Id,
                                Name = q.Name,
                                Code = q.Code
                            })
                    };
                }
            }
            return View(vm);
        }

        [HttpPost, ActionName("New")]
        public ActionResult NewPost(int qId, string qCode, string aCode, QueuerViewModel model)
        {
            var queuer = new Queuer
            {
                QueueCode = qCode,
                Requester = model.Requester,
                Sender = model.Sender,
                SenderType = "manual",
                Status = "pending"
            };
            if (!string.IsNullOrWhiteSpace(queuer.Sender))
            {
                queuer.Subscriptions = new List<Subscription>
                {
                    new Subscription
                    {
                        Destination = model.Sender,
                        Type = "sms"
                    }
                };
            }
            this.queueManager.EnqueueInto(qCode, aCode, queuer);
            return RedirectToAction("Index", "Queues", new { id = qId });
        }
    }
}