﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WaitNoMore.Domain;
using WaitNoMore.Domain.Models;
using WaitNoMore.Domain.Notification;
using WaitNoMore.Web.Models;

namespace WaitNoMore.Web.Controllers
{
    public class QueuesController : Controller
    {
        private readonly INotifier notifier;
        private readonly IQueueManager queueManager;
        private readonly IClientAdministration clientAdministration;

        public QueuesController(
            INotifier notifier,
            IQueueManager queueManager,
            IClientAdministration clientAdministration)
        {
            this.notifier = notifier;
            this.queueManager = queueManager;
            this.clientAdministration = clientAdministration;
        }

        public ActionResult Index(int id)
        {
            var vm = new QueueViewModel();
            var queue = this.queueManager.GetQueueBy(id);
            if (queue != null)
            {
                vm.Id = queue.Id;
                vm.Name = queue.Name;
                vm.Code = queue.Code;
                vm.MostCurrentQueuer = GetMostCurrentQueuer(queue.Queuers);
                vm.Queuers = queue.Queuers.Select(q => new QueuerViewModel
                    {
                        Id = q.Id,
                        Number = q.Number,
                        Requester = q.Requester,
                        Status = q.Status,
                        Sender = q.Sender,
                        SenderType = q.SenderType
                    });

                var account = this.clientAdministration.GetAccountBy(queue.AccountId);
                if (account != null)
                {
                    vm.Account = new AccountViewModel
                    {
                        Id = account.Id,
                        Name = account.Name,
                        Code = account.Code,
                        Queues = account.Queues == null ? Enumerable.Empty<QueueViewModel>() :
                            account.Queues.Select(q => new QueueViewModel
                            {
                                Id = q.Id,
                                Name = q.Name,
                                Code = q.Code
                            })
                    };
                }
            }
            return View(vm);
        }

        public async Task<ActionResult> Next(int id, string qCode, string aCode)
        {
            var queuer = this.queueManager.CallNext(qCode, aCode);
            if (queuer == null)
                return RedirectToAction("Index", new { id = id });

            await this.notifier.SmsQueuerByAsync(queuer.Id, "You're up next!");
            await this.notifier.SmsPendingQueuersAsync(qCode, aCode, "Next queuer has been called.");
            return RedirectToAction("Index", new { id = id });
        }

        public async Task<ActionResult> Skip(int id, int qId, string qCode, string aCode)
        {
            this.queueManager.Skip(id);
            await this.notifier.SmsQueuerByAsync(id, "You have missed your appointment.");
            return RedirectToAction("Next", new { id = qId, qCode = qCode, aCode = aCode });
        }

        public async Task<ActionResult> Ping(int id, int qId)
        {
            await this.notifier.SmsQueuerByAsync(id, "Warning! We're calling your Q number.");
            return RedirectToAction("Index", new { id = qId });
        }

        public ActionResult Complete(int id, int qId)
        {
            this.queueManager.Finish(id);
            return RedirectToAction("Index", new { id = qId });
        }

        private QueuerViewModel GetMostCurrentQueuer(IEnumerable<Queuer> queuers)
        {
            if (queuers == null || !queuers.Any())
            {
                return null;
            }

            var currentQueuers = queuers
                .Where(q => q.Status == "current")
                .OrderByDescending(q => q.Number);
            if (!currentQueuers.Any())
            {
                return null;
            }

            var currentQueuer = currentQueuers.FirstOrDefault();
            return new QueuerViewModel
            {
                Id = currentQueuer.Id,
                Number = currentQueuer.Number
            };
        }
    }
}