﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaitNoMore.Web.Models
{
    public sealed class AccountViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public IEnumerable<QueueViewModel> Queues { get; set; }
    }
}