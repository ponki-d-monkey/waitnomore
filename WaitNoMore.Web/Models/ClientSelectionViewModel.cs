﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WaitNoMore.Web.Models
{
    public sealed class ClientSelectionViewModel
    {
        public int SelectedClient { get; set; }
        public int SelectedAccount { get; set; }

        public IEnumerable<SelectListItem> Clients { get; set; }
        public IEnumerable<SelectListItem> Accounts { get; set; }
    }
}