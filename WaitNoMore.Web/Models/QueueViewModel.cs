﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaitNoMore.Web.Models
{
    public sealed class QueueViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public QueuerViewModel MostCurrentQueuer { get; set; }
        public AccountViewModel Account { get; set; }
        public IEnumerable<QueuerViewModel> Queuers { get; set; }
    }
}