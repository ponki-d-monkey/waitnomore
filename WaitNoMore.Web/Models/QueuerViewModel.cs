﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaitNoMore.Web.Models
{
    public class QueuerViewModel
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Requester { get; set; }
        public string Status { get; set; }
        public string Sender { get; set; }
        public string SenderType { get; set; }
        public QueueViewModel Queue { get; set; }
    }
}